import React, {Component} from 'react';


class App extends Component {
   constructor(props) {
     super(props);
     this.state = {
       itens:[],
       isLoaded: false,

     }  
  }

   componentDidMount() {

      fetch('http://localhost:8000/legacyedu/movimentoscaixa/rest/')
        .then(res => res.json())
        .then(json => {
           this.setState({
             isLoaded: true,
             itens: json,
           })
        });
    }
   
    render(){
      
      var {isLoaded,itens} = this.state;

      if(!isLoaded){
        return <div>Carregando...</div>;
      }

      else{
          
          return (
              <div className="App">
                  
                  <table className =''>
                  
                      {itens.map( item => (
                         <tr key={item.codmovimento}>
                            <tr>codcaixa</tr>
                              { item.codcaixa }

                          </tr>

                      ))}

                  </table>

              </div>
          );
        }
    }
}

export default App;


